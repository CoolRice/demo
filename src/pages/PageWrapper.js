import PageHeader from '../components/PageHeader';
import Main from '../components/Main';
import BaseComponent from '../components/BaseComponent';
import tools from '../util/tools';

class PageWrapper extends BaseComponent{
  constructor(container, options) {
    super(container, options);
    this.render();
  }

  render() {
    const pageWrapper = tools.createElement('div', { id: 'pageWrapper', className: 'page-wrapper'});

    // create sub dom
    const pageHeader = new PageHeader(pageWrapper);
    const main = new Main(pageWrapper);
    pageHeader.render();
    main.render();
    const pageFooter = tools.createElement('footer', { id: 'pageFooter', className: 'page-footer', innerHTML: 'Ⓒ Copyright 2017 Works' });
    pageWrapper.appendChild(pageFooter);

    this.container.appendChild(pageWrapper);
  }
}

export default PageWrapper;