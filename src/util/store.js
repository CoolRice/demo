const initStore = [
  {
    id: 0,
    website: 'bjstdmngbgr01.works.com',
    status: 'idle',
    ip: '192.168.1.102',
    location: '/var/lib/cruise-agent',
    browers: [
      {
        id: 0,
        browerName: 'FireFox'
      },
      {
        id: 1,
        browerName: 'Safari'
      },
      {
        id: 2,
        browerName: 'Ubuntu'
      },
    ],
    deny: true
  },
  {
    id: 1,
    website: 'bjstdmngbgr01.works.com',
    status: 'building',
    ip: '192.168.1.102',
    location: '/var/lib/cruise-agent',
    browers: [
      {
        id: 0,
        browerName: 'FireFox'
      },
      {
        id: 1,
        browerName: 'Safari'
      },
    ],
    deny: false
  },
  {
    id: 2,
    website: 'bjstdmngbgr01.works.com',
    status: 'idle',
    ip: '192.168.1.102',
    location: '/var/lib/cruise-agent',
    browers: [
      {
        id: 0,
        browerName: 'FireFox'
      },
      {
        id: 1,
        browerName: 'Safari'
      },
    ],
    deny: false
  },
];

function createStore(initStore, reducer) {
  var state;
  var listeners = [];

  function getState() {
    return state;
  }

  function subscribe(listener) {
    listeners.push(listener);
    return function unsubscribe() {
      var index = listeners.indexOf(listener);
      listeners.splice(index, 1);
    };
  }

  function dispatch(action) {
    state = reducer(state, action);
    listeners.forEach(listener => listener());
  }

  dispatch({});

  return { dispatch, subscribe, getState };
}

export { createStore, initStore };