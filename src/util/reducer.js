export default (state, action) => {
  switch (action.type) {
  case 'CREATE_NOTE':
  {
    let currentId = state.nextNoteId;
    return {
      nextNoteId: currentId + 1,
      notes: {
        ...state.notes,
        [currentId]: action.content
      },
    };
  }
  case 'UPDATE_NOTE':
  {
    let { id, content } = action;
    return {
      ...state,
      notes: {
        ...state.notes,
        [id]: content
      }
    };
  }
  case 'INIT_NOTE':
  {
    return {
      nextNoteId: 1,
      notes: {}
    };
  }
  default:
    return state;
  }
};