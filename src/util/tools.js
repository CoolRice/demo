function $(selector) {
  if (typeof selector !== 'string') {
    return null;
  }
  return document.querySelector(selector);
}

function createElement(type, options) {
  if (!type) {
    type = 'div';
  }
  const ele = document.createElement(type);
  Object.assign(ele, options);
  return ele;
}

const tools = { $, createElement };

export default tools;