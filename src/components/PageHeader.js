import BaseComponent from './BaseComponent';
import tools from '../util/tools';
import Logo from '../assets/logo/logo.svg';
import Avator from '../assets/osIcons/ubuntu.png';

class PageHeader extends BaseComponent {
  constructor(container, options) {
    super(container, options);
  }

  render() {
    const pageHeader = tools.createElement('header', { id: 'pageHeader', className: 'page-header' });
    pageHeader.innerHTML = `
      <div class="page-header-wrapper">
        <img class="logo" src=${Logo}>
        <div class="profile">
          <img class="avator" src=${Avator}>
          <span class="icon icon-angle-down"></span>
        </div>
      </div>
    `;
    this.container.appendChild(pageHeader);
    // return `
    //   <div class="page-container">
    //     header
    //   </div>
    // `;
  }
}

export default PageHeader;