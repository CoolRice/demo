class BaseComponent {
  constructor(container, options) {
    this.container = container;
    this.options = options;
  }
}

export default BaseComponent;