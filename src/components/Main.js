import SiderBar from '../components/SiderBar';
import BaseComponent from '../components/BaseComponent';
import Agent from './Agent';
import tools from '../util/tools';

class Main extends BaseComponent{
  constructor(container, options) {
    super(container, options);
  }

  render() {
    const main = tools.createElement('main', { id: 'main', className: 'main'});
    const siderBar = new SiderBar(main);
    const agent = new Agent(main);
    siderBar.render();
    agent.render();
    // main.innerHTML = 'main'
    this.container.appendChild(main);
    // return `
    //   <div class="sider-bar">
    //     sider bar
    //   </div>
    // `;
  }

}

export default Main;