import BaseComponent from './BaseComponent';
import tools from '../util/tools';

class Agent extends BaseComponent {
  constructor(container, options) {
    super(container, options);
  }
  render() {
    const agent = tools.createElement('div', { id: 'agent', className: 'current-view agent' });
    agent.innerHTML = `
      <div class="agent-overview">
        <div class="overview-item building">
          <div class="literal">Building</div>
          <div class="icon icon-cog rotating"></div>
          <div class="number">3</div>
        </div>
        <div class="overview-item idle">
          <div class="literal">Idle</div>
          <div class="icon icon-coffee"></div>
          <div class="number">5</div>
        </div>
        <div class="overview-item agent-type">
            <div class="type-item">
              <span class="type-literal">ALL</span>
              <span class="type-number">8</span>
            </div>
            <div class="type-item">
              <span class="type-literal">PHYSICAL</span>
              <span class="type-number">4</span>
            </div>
            <div class="type-item">
              <span class="type-literal">VIRTUAL</span>
              <span class="type-number">4</span>
            </div>
        </div>
      </div>
      <div class="agent-tools">
        <div class="switch-type">
          <div class="type-item current"><span>All</span></div>
          <div class="type-item"><span>Physical</span></div>
          <div class="type-item"><span>Virtual</span></div>
        </div>
        <div class="agent-search">
          <span class="icon-search"></span>
          <input type="text">
        </div>
        <div class="switch-view">
          <div class="icon icon-th-card"></div>
          <div class="icon icon-th-list"></div>
        </div>

      </div>
      <div class="agent-list">list</div>
    `;
    this.container.appendChild(agent);
    // return `
    //   <div class="agent-page">
    //     agent container
    //   </div>
    // `;
  }
}

export default Agent;