import BaseComponent from './BaseComponent';
import tools from '../util/tools';

class SiderBar extends BaseComponent{
  constructor(container, options) {
    super(container, options);
  }

  render() {
    const siderBar = tools.createElement('aside', { id: 'siderBar', className: 'sider-bar'});
    siderBar.innerHTML = `
      <div class="nav">
          <div class="nav-item">
            <span class="nav-icon icon-dashboard"></span><span class="nav-literal">DASHBOARD</span>
          </div>
          <div class="nav-item current">
            <span class="nav-icon icon-sitemap"></span><span class="nav-literal">AGENT</span>
          </div>
          <div class="nav-item">
            <span class="nav-icon icon-boat"></span><span class="nav-literal">MY CRUISE</span>
          </div>
          <div class="nav-item">
            <span class="nav-icon icon-life-bouy"></span><span class="nav-literal">HELP</span>
          </div>
      </div>
      <div class="history">
          <span>History</span>
          <template id="history">
              <ul>
              </ul>
          </template>
      </div>`;
    this.container.appendChild(siderBar);
    // return `
    //   <div class="sider-bar">
    //     sider bar
    //   </div>
    // `;
  }

}

export default SiderBar;